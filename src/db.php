<?php
namespace ivaid;

class db extends \optiwariindia\database
{
    private static $instance;
    public static function create(){
        if(!isset(self::$instance)){
            self::$instance=new db();
        }
        return self::$instance;
    }
    private function  __Construct(){
        $config=include "config.php";
        parent::__Construct($config["db"]);
    }
    public static function save($request,$data){
        $db=self::create();
        $table=$request['type'];
        $info=array_merge([
            "updated_from"=>self::getIP(),
            "updated_by"=>"visitor",
        ],$request);
        unset($info['action']);
        unset($info['type']);
        unset($info['target']);
        unset($info['submit']);
        $status=$db->insert($table,$info);
        $data["save"][]=$status;
        return $data;
    }
    private static function getIP(){return $_SERVER['REMOTE_ADDR'];}
}

