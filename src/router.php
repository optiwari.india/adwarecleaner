<?php
namespace ivaid;

class router
{
    private static $instance;
    private static $template;
    public static function create()
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }
        self::$instance = new router();
        return self::$instance;
    }
    private static function __constructor()
    {
        // self::$project=include "project.php";
    }

    /**
     * Get the value of template
     */
    public function getTemplate()
    {
        $project = include "project.php";
        $uri = $_SERVER['REQUEST_URI'];
        $baseuri = substr($uri, 0, strlen($project['project']['path']));
        if ($baseuri != $project['project']['path']) {
            self::goHome(302);
            return false;
        }
        $root = substr($uri, (strlen($project['project']['path']) + 1), (strlen($uri) - 1));
        $path = explode(DIRECTORY_SEPARATOR, $root);
        if (empty($path[0])) {
            $path[0] = "index";
        }

        return $path[0];
    }
    public function getData()
    {
        $project = include "project.php";
        $uri = $_SERVER['REQUEST_URI'];
        $baseuri = substr($uri, 0, strlen($project['project']['path']));
        if($_SERVER['REQUEST_SCHEME']=='http'){
            self::gotoSSLURL(301);
        }
        if ($baseuri != $project['project']['path']) {
            self::goHome(302);
            return false;
        }
        $root = substr($uri, (strlen($project['project']['path']) + 1), (strlen($uri) - 1));
        $path = explode(DIRECTORY_SEPARATOR, $root);
        $data = $project;

        if ($this->isPost()) {
            $r = $this->getRequest();
            $actions = explode("|", $r['action']);
            foreach ($actions as $action) {
                $data = $this->process($action, $data, $r);
            }
        }
        $data['url']=$path[0];
        if (isset($path[1])) {
            $data = call_user_func_array(["ivaid\\" . $path[0], "open"], [$path[1], $data]);
            
        }

        // console::log($data);
        return $data;
    }
    private function process($action, $data, $request)
    {
        $task = explode("-", $action);
        switch ($task[0]) {
            case 'db':
                $db = db::create();
                $data = call_user_func([$db, $task[1]], $request, $data);
                break;
            case "mail":
                $data = call_user_func([mail::create(), $task[1]], $request, $data);
                console::log($data);
                break;
            default:
                # code...
                break;
        }
        return $data;
    }
    private function getRequest()
    {
        return $_REQUEST;
    }
    private function method()
    {return strtolower($_SERVER['REQUEST_METHOD']);}
    private function isPost()
    {return $this->method() === "post";}
    public static function goHome($error = 301)
    {
        $project = include "project.php";
        switch ($error) {
            case 301:
                header("HTTP/1.1 301 Moved Permanently");
                break;
            case 302:
                header("HTTP/1.1 302 Moved Temporarily");
                break;
            default:
                header("HTTP/1.1 301 Moved Permanently");
                break;
        }
        header("Location: {$project['project']['path']}");
    }
    private static function gotoSSLURL($code){
        $project = include "project.php";
        switch ($error) {
            case 301:
                header("HTTP/1.1 301 Moved Permanently");
                break;
            case 302:
                header("HTTP/1.1 302 Moved Temporarily");
                break;
            default:
                header("HTTP/1.1 301 Moved Permanently");
                break;
        }
        header("Location: https://{$_SERVER['SERVER_NAME']}{$project['project']['path']}");
    }
    public static function setTemplate($template)
    {

    }
}
