<?php
namespace ivaid;

class product
{
    private static $table="products";
    private function getProduct($pname)
    {
        $db = db::create();
        $db->mode(3);
        return $db->select(self::$table, "*", "where name like '{$pname}'");
    }
    private function getProducts()
    {
        $db = db::create();
        $db->mode(3);
        return $db->select(self::$table, "*");
    }
    private function installed(){
        $db = db::create();
        return in_array(self::$table,$db->tables());
    }
    public function open($page, $data)
    {
        if(self::installed()){
            $data['products'] = ($page === '') ? self::getProducts() : self::getProduct($page);
            if(count($data['products'])==0)
            $data['error']=[
                "heading"=>"Product Not found",
                "message"=>"The product you have requested is not available at this moment. Please contact the adminsitrator or try again latter."
            ];
        }else{
            $data['error']=[
                "heading"=>"Missing Module: Product",
                "message"=>"Module product is not properly installed or some of its components are missing. Please check your payment gateway naming convention if the issue persists."
            ];
        }
        return $data;
    }
}
