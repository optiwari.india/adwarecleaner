<?php
namespace ivaid;
class page{
    public function open($page,$data){
        // Aliases
        switch ($page) {
            case 'privacy':
            case 'privacy-policy':
            $page='privacy';
            $data['project']['head']['title'].=" | Privacy Policy";
                break;
            case 'terms':
            case 'terms-condition':
            case 'condition':
                $data['project']['head']['title'].=" | Terms and Conditions";
                $page='terms';
            break;
            case 'cookies' :
            case 'cookie' :
            case 'cookies-policy' :
            case 'cookie-policy' :
                $data['project']['head']['title'].=" | Cookies Policy";
                $page="cookies";
            break;
            case 'sale':
            case 'terms-sales':
            case 'terms-sale':
            case 'sales-terms':
                $data['project']['head']['title'].=" | Terms of Sales";
                $page="sale";
            break;
            default:
                # code...
                break;
        }
        $data['page']="page/".$page;
        return $data;
    }
}