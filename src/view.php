<?php
namespace ivaid;

class view
{
    private static $twig;
    private static $page;
    private static $instance;
    private static $routes=[];
    public static function create($view)
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }
        self::$instance = self::__constructor($view);//new \ivaid\view($view);
        return self::$instance;
    }
    private static function __constructor($viewpath)
    {
        $loader = new \Twig\Loader\FilesystemLoader($viewpath);
        $twig = new \Twig\Environment($loader, ['debug' => true]);
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        self::$twig = $twig;
    }
    public static function render($data)
    {
        if (!isset(self::$twig)) {
            return false;
        }
        if(isset($data['error']))
            return self::$twig->render("error.twig",$data);
        return self::$twig->render(self::$page, $data);
    }
    public static function template($template)
    {
        self::$page = $template;
    }
}
