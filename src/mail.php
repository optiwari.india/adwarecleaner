<?php
namespace ivaid;

class mail
{
    private static $instance;
    private  $mailer;
    private  $template;
    private $config;
    public static function create()
    {
        if (!isset(self::$instance)) {
            self::$instance = new mail();
        }
        return self::$instance;
    }
    private function __Construct()
    {
        $config = include "config.php";
        $this->config = $config['mail'];
        $this->mailer = new \PHPMailer\PHPMailer\PHPMailer();
        $loader = new \Twig\Loader\FilesystemLoader($config['mailpath']);
        $twig = new \Twig\Environment($loader, ['debug' => true]);
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        $this->template = $twig;
    }
    public function send($info)
    {
        $config = $this->config;
        $mail = $this->mailer;
        if (strtolower($config['type'] ?? "") == "smtp") {
            $mail->isSMTP();
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        }

        if (strtolower($config['debug'] ?? "") == "active") {
            $mail->SMTPDebug = 4;// \PHPMailer\PHPMailer\SMTP::DEBUG_SERVER;
        }

        if (strtolower($config['host'] ?? "") != "") {
            $mail->Host = $config['host'];
        }

        if (strtolower($config['port'] ?? "") != "") {
            $mail->Port = $config['port'];
        }

        if (strtolower($config['tls'] ?? "") != "") {
            $mail->SMTPSecure = \PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_STARTTLS;
        }
        

        if (strtolower($config['user'] ?? "") != "") {
            $mail->SMTPAuth = true;
            $mail->Username = $config['user'];
            $mail->Password = $config['pass'];
            $mail->setFrom($config['from'], $info['from']);
            $mail->addReplyTo($info['reply'], $info['from']);
        }
        foreach ($info['to'] as $user) {
            $mail->addAddress($user['mail'], $user['name']);
        }
        $mail->Subject = $info['subject'];
        $mail->Body = $this->template->render($info['html'], $info);
        $mail->AltBody = $this->template->render($info['txt'], $info);
        
        if (!$mail->send()) {
            return 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            return 'Message sent!';
        }

    }
    public function staff($request, $data)
    {
        $info = [
            "from" => $request['fname'] . " " . $request['lname'],
            "reply" => $request['email'],
            "subject" => "Enquiry From Contact Page",
            "data" => [
                $request, $data,
            ],
            "to" => [
                [
                    "mail" => "optiwari.india@gmail.com",
                    "name" => "Om Prakash Tiwari",
                ]
            ],
            "html"=>"html/enquiry.twig",
            "txt"=>"txt/enquiry.twig"
        ];
        $data['email']['staff']=$this->send($info);
        return $data;
    }
    public function ack($request, $data)
    {
        
        return $data;
    }
}
