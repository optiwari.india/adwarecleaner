<?php
return [
    "project" => [
        "contact"=>[
            "address"=>[
                "link"=>"",
                "text"=>"3683  Gordon Street, Muscoy, California, 92407 United States"
            ],
            "phone"=>[
                "link"=>"tel:+18888888888",
                "text"=>"+1 (888)888-8888"
            ],
            "email"=>[
                "link"=>"mailto:contact@adwarecleaner.pro",
                "text"=>"contact@adwarecleaner.pro"
            ]
        ],
        "name" => "Adware Cleaner",
        "path" => "",
        "fullname" => "Adware Cleaner",
        "purchase" => "/product",
        "head"=>[
            "keywords"=>"Adware Cleaner, Ads, Internet Speed, Clean, Spams,popup blocker, popup cleaner",
            "description"=>"Get rid of all annoying adwares, popups and spams",
            "title"=>"Adware Cleaner",
            "image"=>"",
            "googlesiteverification"=>"",
        ],
        "features" => [
            "summary" => "Since the dawn of time man has sought ways to avoid being bombarded by ads at every turn.  Even the caveman of yesteryear would consistently run into cave drawings promising rounder wheels or hotter fire.",
            "list" => [
                [
                    "icon" => "flaticon-diamond",
                    "name" => "Elegant Design",
                    "text" => "The great design makes it easy to work and helps you feel plesent while working on it.",
                ],
                [
                    "icon" => "flaticon-eye",
                    "name" => "Crushes Annoying Software",
                    "text" => "Removes unwanted browser toolbars, spywares and Pop-ups that run behind screen",
                ],
                [
                    "icon" => "flaticon-update-arrows",
                    "name" => "Quick Update",
                    "text" => "Updates on weekly basis is provided in minimized format to reduce bandwidth consumption while updating.",
                ],
                [
                    "icon" => "flaticon-avatar",
                    "name" => "User Friendly",
                    "text" => "Nicely placed icons and buttons makes it easier for every user. The placement of buttons are made customizable for ease of access",
                ],
                [
                    "icon" => "flaticon-programming",
                    "name" => "Scans Faster",
                    "text" => "Our team is continuously working on to the software to regularly improve it's performance and efficiency.",
                ],
                [
                    "icon" => "flaticon-shield",
                    "name" => "100% Secure",
                    "text" => "The software blocks all malicious content and makes your system 100% malware free",
                ],
            ],
        ],
        "facts" => [
            "summary" => "Completely disintermediate excellent skills with client contents",
            "list" => [
                [
                    "icon" => "flaticon-cloud-computing",
                    "count" => 4036,
                    "text" => "We have more than 5000 happy customers enjoying our service accross USA",
                ],
                [
                    "icon" => "flaticon-checked",
                    "count" => 1250,
                    "text" => "We are recommended by more than thousands of organizations",
                ],
                [
                    "icon" => "flaticon-trophy",
                    "count" => 75,
                    "text" => "We have made a reach to 75 countries accross the world.",
                ],
            ],
        ],
        "plans" => [
            [
                "icon" => "flaticon-house",
                "name" => "Basic",
                "features" => [
                    "One Time Setup","Block Popups", "Block Ads", "Block Malware", "Block Spyware", "Automatic Update", "Online Support",
                ],
                "price" => 99,
                "link" => "basic-single-device",
            ],

            [
                "icon" => "flaticon-skyline",
                "name" => "Standard",
                "features" => [
                    "Single Device","Block Popups", "Block Ads", "Block Malware", "Block Spyware", "Automatic Update", "Online Support",
                ],
                "price" => 159,
                "link" => "basic-2-device",
            ],

            [
                "icon" => "flaticon-university",
                "name" => "Premium",
                "features" => [
                    "Single Device + ","Block Popups", "Block Ads", "Block Malware", "Block Spyware", "Automatic Update", "Online Support",
                ],
                "price" => 199,
                "link" => "premium-multidevice",
            ],

        ],
        "testimonials" => [
            [
                "img" => "1.png",
                "name" => "Alice",
                "role" => "App Developer",
                "text" => "My internet speed has been increased after using this software.",
            ],
            [
                "img" => "2.png",
                "name" => "Mark Parker",
                "role" => "Marketing Manager",
                "text" => "Got freedom from unwanted notifications and advertisement.",
            ],
            [
                "img" => "3.png",
                "name" => "Ilina Dcruz",
                "role" => "Entrepreneur",
                "text" => "Got my internet faster that I expected. Increase in productivity of my team also.",
            ],
        ],
        "preloads" => [
            "/fonts/fontawesome-webfont.woff2",
            "/fonts/Flaticon.woff",
            "/fonts/pxiGyp8kv8JHgFVrJJLucHtA.woff2",
            "/fonts/pxiByp8kv8JHgFVrLDz8Z1xlFQ.woff2",
            "/fonts/pxiEyp8kv8JHgFVrJJfecg.woff2",
            "/fonts/pxiByp8kv8JHgFVrLGT9Z1xlFQ.woff2",
            "/fonts/pxiEyp8kv8JHgFVrJJbecmNE.woff2",
            "/fonts/pxiByp8kv8JHgFVrLEj6Z1xlFQ.woff2"
        ],
        "socials" => [
            [
                "class" => "fa fa-facebook",
                "url" => "https://facebook.com/",
                "name"=>"facebook"
            ],
            [
                "class" => "fa fa-twitter",
                "url" => "https://twitter.com/",
                "name"=>"twitter"
            ],
            [
                "class" => "fa fa-linkedin",
                "url" => "https://linkedin.com/",
                "name"=>"linkedin"
            ],
            [
                "class" => "fa fa-instagram",
                "url" => "https://instagram.com/",
                "name"=>"instagram"
            ],
        ],
        "year" => "2020",
        "page"=>[
            [
                "name"=>"Privacy Policy",
                "path"=>"privacy-policy"
            ],
            [
                "name"=>"Terms and condition",
                "path"=>"terms-condition"
            ],
            [
                "name"=>"Cookies Policy",
                "path"=>"cookies-policy"
            ],
            [
                "name"=>"Terms of sale",
                "path"=>"terms-sales"
            ],
            [
                "name"=>"Return Policy",
                "path"=>"return"
            ]
        ]
    ],
];