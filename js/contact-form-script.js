$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
		formError();
    } else {
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var data={
        fname : $("#fname").val(),
        lname : $("#lname").val(),
        email : $("#email").val(),
        message : $("#message").val(),
        action:$("[name=action]").val(),
        type:$("[name=type]").val()
    }
    var target=$("[name=target]").val();


    $.ajax({
        type: "POST",
        url: target,
        data: data,
        success : function(text){
            resp=JSON.parse(text);
            if (resp.save[0]['result']){
                formSuccess();
            } else {
                formError();
                // submitMSG(false,text);
            }
        }
    });
    return false;
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "You mail has been sent successfully!")
}
function formError(){
    $("#contactForm").removeClass().addClass('animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}
function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "text-success";
    } else {
        var msgClasses = "text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}