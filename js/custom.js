const XHR = {
  loadScript: function (path) {
    let xhr = new XMLHttpRequest()
    console.log(path);
    xhr.open('GET', path);
    xhr.responseType = 'text';
    xhr.send();
    xhr.onload = ()=>{
      scr = document.createElement('script')
      scr.innerHTML=xhr.response;
      document.querySelector('body').append(scr)
    }
  },
  loadStyle: function(path){
    new Promise(res=>{

      let xhr = new XMLHttpRequest()
      console.log(path);
      xhr.open('GET', path);
      xhr.responseType = 'text';
      xhr.send();
      xhr.onload = ()=>{
        css = document.createElement('style')
        css.innerHTML=xhr.response;
        document.head.append(css)
        res(true);
      }
    })
  }
}
const styles = {
  list: [ 'style1.css', 'product.css'],
  load: ()=> {
    return new Promise((resolve) => {
      for (let index = 0; index < styles.list.length; index++) {
        const element = styles.list[index]
        // lnk = document.createElement('link')
        // lnk.setAttribute('href', '/css/' + element)
        // lnk.setAttribute('rel', 'stylesheet')
        // document.head.append(lnk)
        XHR.loadStyle("/css/"+element);
      }
      resolve('done')
    })
  },
}
const scripts = {
  list: [
    'jquery-3.3.1.min.js',
    'popper.min.js',
    'bootstrap.min.js',
    'jquery.counterup.min.js',
    'waypoints.min.js',
    'jquery.easing.min.js',
    'wow.js',
    'tilt.jquery.js',
    'owl.carousel.min.js',
    'form-validator.js',
    'contact-form-script.js',
    'script.js'
  ],
  load: function () {
    return new Promise((resolve) => {
      for (let index = 0; index < scripts.list.length; index++) {
        const element = scripts.list[index]
        XHR.loadScript("/js/"+element);
        // scr.setAttribute('src', '/js/' + element)
      }
      resolve('done')
    })
  }
}
function loadPaymentGateway() {
  var s = document.createElement('script')
  s.setAttribute('id', 'fsc-api')
  s.setAttribute('data-storefront', 'ivaid.test.onfastspring.com/popup-adware')
  s.src =
    'https://d1f8f9xcsvx3ha.cloudfront.net/sbl/0.8.3/fastspring-builder.min.js'
  document.querySelector('body').append(s)
}
window.addEventListener('load', () => {
  console.log('Loading scripts')
  // styles.load();
  // scripts.load().then(styles.load())
  // console.log("Scripts Loaded... Loading Styles");
  // await styles.load();
  loadPaymentGateway()
})
