<?php
// echo json_encode($_SERVER);die;
call_user_func(function () {
    $temp = explode(PATH_SEPARATOR, get_include_path());
    $temp[] = __DIR__;
    set_include_path(implode(PATH_SEPARATOR, $temp));
});
require_once "vendor/autoload.php";
use ivaid\view;
use ivaid\router;
$router=router::create();

$view = view::create(__DIR__ . DIRECTORY_SEPARATOR . 'views');
$data=$router->getData();
// \ivaid\console::log($data);
view::template($router->getTemplate().".twig");

echo view::render($data);
