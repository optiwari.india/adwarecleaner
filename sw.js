/*
Copyright 2018 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js',
)

if (workbox) {
  workbox.precaching.precacheAndRoute([
    { url: '/fonts/fontawesome-webfont.woff2', revision: 1 },
    { url: '/fonts/Flaticon.woff', revision: 1 },
    { url: '/fonts/pxiGyp8kv8JHgFVrJJLucHtA.woff2', revision: 1 },
    { url: '/fonts/pxiByp8kv8JHgFVrLDz8Z1xlFQ.woff2', revision: 1 },
    { url: '/fonts/pxiEyp8kv8JHgFVrJJfecg.woff2', revision: 1 },
    { url: '/fonts/pxiByp8kv8JHgFVrLGT9Z1xlFQ.woff2', revision: 1 },
    { url: '/fonts/pxiEyp8kv8JHgFVrJJbecmNE.woff2', revision: 1 },
    { url: '/fonts/pxiByp8kv8JHgFVrLEj6Z1xlFQ.woff2', revision: 1 },
    { url: '/css/style.css', revision: 1 },
    { url: '/css/style1.css', revision: 1 },
    { url: '/css/product.css', revision: 1 },
    { url: '/', revision: 1 },
    { url: '/js/jquery-3.3.1.min.js', revision: 1 },
    { url: '/js/popper.min.js', revision: 1 },
    { url: '/js/bootstrap.min.js', revision: 1 },
    { url: '/js/jquery.counterup.min.js', revision: 1 },
    { url: '/js/waypoints.min.js', revision: 1 },
    { url: '/js/jquery.easing.min.js', revision: 1 },
    { url: '/js/wow.js', revision: 1 },
    { url: '/js/tilt.jquery.js', revision: 1 },
    { url: '/js/owl.carousel.min.js', revision: 1 },
    { url: '/js/form-validator.js', revision: 1 },
    { url: '/js/contact-form-script.js', revision: 1 },
    { url: '/js/script.js', revision: 1 },
    { url: '/img/logo.png', revision: 1 },
    { url: '/img/laptop.png', revision: 1 },
    { url: '/img/download.png', revision: 1 },
    { url: '/img/1.png', revision: 1 },
    { url: '/img/2.png', revision: 1 },
    { url: '/img/3.png', revision: 1 },
    { url: '/img/contact-img.png', revision: 1 },
    { url: '/img/payment-option.png', revision: 1 },
    { url: '/img/banner-bg.png', revision: 1 },
    { url: '/img/footer-bg.png', revision: 1 },
    { url: '/manifest.json', revision: 1 },
    { url: '/page/privacy-policy', revision: 1 },
    { url: '/page/terms-condition', revision: 1 },
    { url: '/page/cookies-policy', revision: 1 },
    { url: '/page/terms-sales', revision: 1 },
  ])

  workbox.routing.registerRoute(
    /(.*)(?:png|gif|jpg|woff2|js|css)/,
    workbox.strategies.cacheFirst({
      cacheName: 'images-cache',
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 50,
          maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
        }),
      ],
    }),
  )

  const articleHandler = workbox.strategies.networkFirst({
    cacheName: 'api',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
      }),
    ],
  })

  workbox.routing.registerRoute(/page(.*)/, (args) => {
    return articleHandler.handle(args).then((response) => {
      if (!response) {
        return caches.match('page/offline')
      } else if (response.status === 404) {
        return caches.match('page/404')
      }
      return response
    })
  })

  const postHandler = workbox.strategies.cacheFirst({
    cacheName: 'posts-cache',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
      }),
    ],
  })

  workbox.routing.registerRoute(/(.*)/, (args) => {
    return postHandler
      .handle(args)
      .then((response) => {
        if (response.status === 404) {
          return caches.match('page/404.html')
        }
        return response
      })
      .catch(function () {
        return caches.match('page/offline.html')
      })
  })
} else {
  console.log(`Boo! Workbox didn't load 😬`)
}
